import * as Smartraise from "../artifacts/Smartraise.json";
import { Smartraise as TypeSmartRaise } from '../../web3transactions/typechain/Smartraise'
import { TransactionManager } from "../transactionManager/TransactionManager.js";

export async function deployCampaign(tM: TransactionManager) {

    let web3Signer = await tM.getWeb3Signer();
    
    // Put together transaction
    var newContract = new web3Signer.eth.Contract(Smartraise.abi) as TypeSmartRaise
    const transaction = newContract.deploy({ data: Smartraise.bytecode, arguments: [] })
 
    tM.processTransaction(transaction, "Deploy New Contract")

}
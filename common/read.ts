// import { verifyContractOnAddress } from "./verify";

/// Using bluebird promises, they can be canceled and timeout
// Note: craps itself at Metamask wallet unlock
import * as BBPromise from "bluebird";
BBPromise.config({ cancellation: true });


type Unpromise<T> = T extends Promise<infer U> ? U : never;
// type ResolvedObject<T> = { [P in keyof T]: Unpromise<T[P]>; }

type contractReadReturn = {contractValues: any, contractErrors: string}

/** Takes an Object with method promises and processes them  
 * @param contractMethods Map with Keys: Name, Values: method references
 * @returns contractValues: Key/Value Map or
 * @returns contractErrors: Error Message
*/
export async function readContractData<T>(web3, contractArtifact, contractAddress, contractMethods: T) {
    // TODO: Contract verification taken out, because bytecode changes too frequently (compilter updates, etc). 
    // Needs alternative handling if some contract methods are not available 
    try {
        const methodResults = await BBPromise.props(contractMethods).timeout(2000) as { [P in keyof T]: Unpromise<T[P]>; }
        return ({
            contractValues: methodResults,
        })
    } catch (error) {
        return {
            contractErrors: "Error occured: " + error.message
        }
    }
}
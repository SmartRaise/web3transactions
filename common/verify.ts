import * as Smartraise from "../artifacts/Smartraise.json";

/** Checks address and deployed bytecode. Returns true if everything is cool, and an error message if not */
async function compareBytecodeOnAddress(web3, expectedBytecode, address): Promise<true | string> {
    var deployedByteCode;

    try {
        deployedByteCode = await web3.eth.getCode(address)
    } catch (err) {
        return err.message
    }
    finally {
        if (deployedByteCode == "0x0" || deployedByteCode == "0x") {
            return "There is no contract deployed at that address."
        }
        if (deployedByteCode != expectedBytecode) {

            return true
            // This is too strict for now, code changes too often (truffle update, compilers, etc)
            // return "The contract at the address is different than expected."
        }
        if (deployedByteCode == expectedBytecode) {
            return true;
        }
    }

    throw ("compareByteCode: Unhandled deployedByteCode -" + deployedByteCode)
}

/** Checks address and deployed bytecode. Returns true if everything is cool, and an error message if not */
export async function verifyContractOnAddress(web3, contractDeployedBytecode, contractAddress): Promise<true | string> {

    if (!web3.utils.isAddress(contractAddress)) {
        return "This is not a valid Ethereum Address"
    }
    else {
        return compareBytecodeOnAddress(web3, contractDeployedBytecode, contractAddress)
    }
}

/** Checks for Smartraise contract on address */
export async function verifySmartraiseOnAddress(web3, contractAddress): Promise<true | string> {
    return verifyContractOnAddress(web3, Smartraise.deployedBytecode, contractAddress)
}
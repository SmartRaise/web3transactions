import { observable, action, computed, runInAction } from 'mobx';
import { createTransformer } from 'mobx-utils'
import { clearConfigCache } from 'prettier';
if (!fetch) { var fetch = require("node-fetch"); } // No fetch on node

const FXAPIURL = "https://api.bitpanda.com/v1/ticker"

export class FxStore {
    /* tried to have these observable, but then I get Uncaught TypeError: 
    observable$$1.observers.add is not a function when reading it on project pages */
    fxRate = 0;
    fxSymbol = "EUR";

    constructor() {
        console.log("ASDF")
        this.updateFx()
    }

    @action
    async updateFx() {
        let res = await fetch(FXAPIURL)
        let json = await res.json()

        // runInAction(() => {
        this.fxRate = json.ETH.EUR
        // })
        console.log("ETHEUR", this.fxRate)
    }

    // @computed get computedEtherToEur() {
    //     return createTransformer(
    //         (valueInEth: number) => {
    //             console.log("Create transformer Fx:" + this.fxRate + " Value: " + valueInEth)
    //             return valueInEth * this.fxRate
    //         })
    // }

    round5(x) {
        return Math.ceil(x / 5) * 5;
    }

    etherToEur(valueInEth: number) {
        console.log("Create transformer Fx:" + this.fxRate + " Value: " + valueInEth)
        return valueInEth * this.fxRate
    }

    etherToEurRound5(valueInEth){
        return this.round5(this.etherToEur(valueInEth))
    }

}
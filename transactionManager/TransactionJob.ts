import Contract from "web3/eth/contract";
import PromiEvent from "web3/promiEvent";
import { observable, action } from "mobx";

export enum TransactionStatus {
    NEW,
    HASH,
    RECEIPT,
    ERROR
}
/**
 * Transaction Jobs hold data about the long running transaction promises as well as
 * their results. (error/receipt). They expose callbacks which are fed to the promises at start.
 */
export class TransactionJob {
    // Note: If you don't use status, make something else observable, so it triggers!
    label: String;
    hash: string;
    @observable
    status: TransactionStatus;
    statusMessage: string;
    receipt: any;
    promise: PromiEvent<Contract | void>;

    @action.bound
    onReceipt(receipt) {
        this.receipt = receipt;
        this.status = TransactionStatus.RECEIPT;
        console.log("Got Receipt: " + JSON.stringify(receipt));
    }

    @action.bound
    onHash(hash) {
        this.hash = hash;
        this.status = TransactionStatus.HASH;
        console.log("Got Hash");
    }

    @action.bound
    onError(error) {
        this.statusMessage = error;
        this.status = TransactionStatus.ERROR;
        console.log("Got Error");
    }
    
    constructor(label: String) {
        this.label = label;
        this.hash = "";
        this.status = TransactionStatus.NEW;
        this.statusMessage = "";
        this.receipt = null;
    }
}

import Contract from "web3/eth/contract";
import * as Smartraise from "../artifacts/Smartraise.json";
import { Smartraise as TypeSmartRaise } from '../typechain/Smartraise'

import { TransactionObject } from "web3/eth/types";
import * as unlockWallet from "./unlockWallet";

import { observable, computed, reaction, action } from "mobx";
import { TransactionJob } from "./TransactionJob";
import { stringify } from "querystring";

/**
 * Stores List of Transaction Jobs. Provides functions to create, sign and send transactions.  
 */
export class TransactionManager {

    /**
     *  Store for Transaction Jobs 
     */
    @observable
    TransactionJobs: Array<TransactionJob> = [];

    /** Creates an empty Transaction Job  */
    @action
    newTransactionJob(label: String) {
        const trans = new TransactionJob(label);
        this.TransactionJobs.push(trans);
        try {
            console.log("NewTrans! " + JSON.stringify(this.TransactionJobs));
        }
        catch (e) {  // This is for circular structures
            console.log(e)
        }

        return trans;
    }

    /**
     * Returns an unlocked instance of Metamask ready for signing. Don't store it as it might expire / get invalid.  
     * Convenience function for cleaner scope
     */
    async getWeb3Signer() {
        return await unlockWallet.getWeb3Signer();
    }

    /**
     * Loads from chain and instatiates a SmartRaise contract. 
     * TODO: This is the only SmartRaise specific part in the TM, could be refactored out.  
     * @param contractAddress 
     */
    async getSmartRaiseContract(contractAddress) {
        let web3Signer = await this.getWeb3Signer();
        var newContract = new web3Signer.eth.Contract(Smartraise.abi, contractAddress) as TypeSmartRaise
        return newContract;
    }

    /**
     * Processes Transactions: 1) Unlocks signer, 2) Creates Transaction Job, 
     * 3) Estimates gas, 4) Sets up and fires long running promise
     * 
     * @param transaction 
     * @param label 
     */
    async processTransaction(transaction: TransactionObject<Contract | void>, label: String = "") {
        const web3signer = await this.getWeb3Signer()

        // Set up TransactionJob
        const newTrans = this.newTransactionJob(label);

        // Run Transaction
        var gasEstimate;
        try {
            gasEstimate = await transaction.estimateGas({
                from: web3signer.eth.defaultAccount
            });
        }
        catch (e) {
            // This is where you end up with code -32603. Sadly, we can't check for that, because 
            // we only get a string error message from Metamask, but not the code.

            const explanation = `Error while trying to estimate gas. If you see the message Internal JSON-RPC Error, -32603,  \
            that's just a catch-all error, which could be the result of a VM revert, or a mismatch with your contracts. Most probable reasons \
            1) You tried to do something that's not allowed in the contract. (Any VM revert) 2) The contract and the artifact JSON have a mismatch, \
            3) Something is wrong with infura, metamask or your provider.`

            console.log(explanation + JSON.stringify(e))

            // TODO: Figure out why strings get swallowed if passed in here       
                                           
            newTrans.onError(e)
        };



        if (gasEstimate) try {
            newTrans.promise = transaction
                .send({
                    from: web3signer.eth.defaultAccount,
                    gas: gasEstimate
                })
                .on("error", newTrans.onError)
                .on("transactionHash", newTrans.onHash)
                .on("receipt", newTrans.onReceipt); // contains the new contract address

            // .on('confirmation', function (confirmationNumber, receipt) { console.log("ConfiNumber " + confirmationNumber + receipt) })
            // .then(function (newContractInstance) {
            //     console.log(newContractInstance.options.address) // instance with the new contract address
            // })); 
        }
            // Here we catch Metamask errors, like -jk
            catch (e) {
                console.log("Exception in processTransaction when trying to send transaction:" + JSON.stringify(e))
                newTrans.onError(e);
            }
    }
}

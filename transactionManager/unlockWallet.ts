import Web3 = require("web3");

// TODO: refactor to make non-global
declare global {
    interface Window {
        ethereum: any;
        web3: Web3;
    }
}

/**
 * Returns an unlocked instance of Metamask ready for signing. Don't store it as it might expire / get invalid.  
 */
export async function getWeb3Signer() {
    // Set up Wallet
    const web3Signer = await walletEnable();

    if (web3Signer) {
        let accounts = await web3Signer.eth.getAccounts();
        web3Signer.eth.defaultAccount = accounts[0];
    }
    return web3Signer;
}



// TODO: Test with other browsers
export async function walletEnable() {
    var web3Signer: Web3 | null = null;
    // Metamask v4.14.0+
    if (window.ethereum) {
        try {
            if (typeof window.ethereum.selectedAddress === "undefined") {
                // Request account access if needed
                // Note: MetaMask 5.2.2 on Iridium sometimes silently fails at enable() and never returns promise.
                // When refreshing the page, it works. Randomly works, difficult to reproduce
                // Definetly doesnt work with Bluebird Promises, otherwise unclear
                await window.ethereum.enable(); // Fails in MM 5.2.2

                console.log("Enabled!");
            }
            web3Signer = new Web3(window.ethereum);
            // Acccounts now exposed
        } catch (error) {
            console.log("Error while enabling Wallet:" + error);
            // TODO: User denied account access...
        }
    }
    // Legacy dapp browsers...
    else if (window.web3) {
        web3Signer = new Web3(window.web3.currentProvider);
        // Acccounts always exposed
    }
    // Non-dapp browsers...
    else {
        // TODO: No signature wallet
        web3Signer = null;
        console.log(
            "Non-Ethereum browser detected. You should consider trying MetaMask!"
        );
    }

    return web3Signer;
}

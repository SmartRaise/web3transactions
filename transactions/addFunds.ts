import * as UnlockWallet from "../transactionManager/unlockWallet";
import Web3 = require("web3");

// export async function addFunds(tM: TransactionManager, contractAddress) {
//     const contract = await tM.getSmartRaiseContract(contractAddress);
//     const transaction = contract.methods.setHold();

//     tM.processTransaction(transaction, "Set Status Hold")
// }


// We do this very, very simple now. For proper signature / transaction management we could 
// use the TransactionManager. But let's start simple.
export async function addFunds(recipient: string, valueEth: string) {
    let value = Web3.utils.toWei(valueEth, "ether")

    let web3signer = await UnlockWallet.getWeb3Signer();

    // debugger

    if (web3signer) {
        var res = await web3signer.eth.sendTransaction({ from: web3signer.eth.defaultAccount, to: recipient, value: value })
        return res
    }
    else {
        return null
    }
}
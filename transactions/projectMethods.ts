import { Smartraise as TypeSmartRaise } from '../typechain/Smartraise'
import * as Smartraise from "../artifacts/Smartraise.json";

export type Unpromise<T> = T extends Promise<infer U> ? U : never;
export type ResolvedObject<T> = { [P in keyof T]: Unpromise<T[P]>; }
export type projectType = ResolvedObject<ReturnType<typeof projectMethods>> & { contractAddress?: string }

export function projectMethods(web3, contractAddress) {
    // Instatiate Smart Contract
    const smartContract =
        new web3.eth.Contract(Smartraise.abi, contractAddress) as TypeSmartRaise

    // Build a Key-Method Object for Smart Contract Calls
    const scm = smartContract.methods

    return {
        projectId: scm.projectId().call(),
        statusFunding: scm.statusFunding().call(),
        statusProject: scm.statusProject().call(),

        countPhases: scm.countPhases().call(),
        activePhase: scm.activePhase().call(),

        totalGoal: scm.totalGoal().call(),
        totalCollected: scm.totalCollected().call(),
        countSupporters: scm.countSupporters().call(),
    }
}

export function projectPayoutMethods(web3, contractAddress) {
    // Instatiate Smart Contract
    const smartContract =
        new web3.eth.Contract(Smartraise.abi, contractAddress) as TypeSmartRaise

    // Build a Key-Method Object for Smart Contract Calls
    const scm = smartContract.methods

    return {
        leftToPayoutLimit: scm.leftToPayoutLimit().call(),
        approvedPayoutLimit: scm.approvedPayoutLimit().call(),
        unlockedFunding: scm.unlockedFunding().call()
    }
}
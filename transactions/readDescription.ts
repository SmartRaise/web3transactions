import BigNumber from "bignumber.js"
import Web3 = require("web3");

import { readContractData } from '../common/read'
import { Smartraise as TypeSmartRaise } from '../typechain/Smartraise'
import * as Smartraise from "../artifacts/Smartraise.json";

// export type PhaseType = ReturnType<typeof phaseToObject>
// export type PhaseMethodsType = ReturnType<typeof phaseMethods>

export type DescriptionType = {
    title: string,
    aboutProject: string,
    aboutUs: string,
    images: Array<string>
}


function resultsToObject(rawDescription) {
    // console.log("ResultsToObjects before: " + JSON.stringify(rawDescription))
    
    var obj;

    if (rawDescription.contractValues && rawDescription.contractValues.description.payload) {
        const cvp = JSON.parse(rawDescription.contractValues.description.payload)
        obj = {
            title: cvp.title,
            aboutProject: cvp.aboutProject,
            aboutUs: cvp.aboutUs,
            images: cvp.images
        }
    } else {
        obj = {
            title: "",
            aboutProject: "",
            aboutUs: "",
            images: []
        }
    }

    // console.log("ResultsToObjects after: " + JSON.stringify(obj))
    return obj
}

/** These are the contract methods used to read content */
export async function readDescription(web3, contractAddress) {
    // Instatiate Smart Contract
    const smartContract =
        new web3.eth.Contract(Smartraise.abi, contractAddress) as TypeSmartRaise

    const descriptionPromObj =
        { description: smartContract.methods.getDescription().call() }

    const descriptionResults =
        await readContractData(web3, Smartraise, contractAddress, descriptionPromObj)

    return resultsToObject(descriptionResults)
}


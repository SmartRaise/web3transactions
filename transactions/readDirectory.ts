import BigNumber from "bignumber.js"
import Web3 = require("web3");

import { readContractData } from '../common/read'
import { Directory as TCDirectory } from '../typechain/Directory'
import * as Directory from "../artifacts/Directory.json";

/** These are the contract methods used to read content */
export async function readDirectory(web3, contractAddress) {
    // Instatiate Smart Contract
    const directoryContract =
        new web3.eth.Contract(Directory.abi, contractAddress) as TCDirectory

    const directoryPromObj =
        { directory: directoryContract.methods.getCampaigns().call() }

    const directoryResults =
        await readContractData(web3, Directory, contractAddress, directoryPromObj)

    console.log("Directory: ", JSON.stringify(directoryResults))
    // console.log("dirstuff ", + JSON.stringify(directoryResults))
    // return resultsToObject(directoryResults)
    return directoryResults;
}


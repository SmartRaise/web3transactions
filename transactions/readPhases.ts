import BigNumber from "bignumber.js"
import Web3 = require("web3");

import { readContractData } from '../common/read'
import { Smartraise as TypeSmartRaise } from '../typechain/Smartraise'
import * as Smartraise from "../artifacts/Smartraise.json";

export type PhaseType = ReturnType<typeof phaseToObject>
export type PhaseMethodsType = ReturnType<typeof phaseMethods>

/** These are the contract methods used to read phases */
export function phaseMethods(web3, contractAddress) {
    // Instatiate Smart Contract
    const smartContract = new web3.eth.Contract(Smartraise.abi, contractAddress) as TypeSmartRaise

    // Build a Key-Method Object for Smart Contract Calls
    const scm = smartContract.methods

    // Build Key-Method object for getPhase calls
    return {
        getPhase: scm.getPhase
    }
}

export async function readPhaseData(web3, contractAddress, contractPhaseMethods: PhaseMethodsType, nrOfStages: number) {


    // Prepare Promise Object: Key holds phase number, Value holds call on contract
    var phasesPromObj = [];
    for (let i = 0; i < nrOfStages; i++) {
        phasesPromObj.push(contractPhaseMethods.getPhase(i).call());
    }

    // Query contract 
    const phasesResults = await readContractData(web3, Smartraise, contractAddress, phasesPromObj)

    console.log("Phase raw: " + JSON.stringify(phasesResults))


    if (phasesResults.contractValues) {
        let cV = phasesResults.contractValues; // shorten for readability

        // Map from object keys to array, and clean up phase data at the same time
        return Object.keys(cV).map(key => phaseToObject(web3, cV[key]))
    }
    // TODO: Error Handling
}

/** Here we map the raw data to JS objects. 
 * The output of this function defines the type of PhaseType 
 * */
function phaseToObject(web3, rawData) {

    console.log("Phase raw: " + JSON.stringify(rawData))

    return {
        label: web3.utils.toUtf8(rawData.label),
        goal: new BigNumber(rawData.goal),
        dueDate: parseInt(rawData.dueDate),
        accumulatedGoal: new BigNumber(rawData.accumulatedGoal),
    }
}
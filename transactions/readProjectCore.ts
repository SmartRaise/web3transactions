import { readContractData } from '../common/read'
import * as Smartraise from "../artifacts/Smartraise.json";
import * as PhasesModel from './readPhases'
import * as ProjectModel from './projectMethods'

import * as DescriptionModel from './readDescription';

export type projectFullType =
    ProjectModel.projectType &
    { phases?: Array<PhasesModel.PhaseType> } &
    { description?: any }
    // { description?: DescriptionModel.DescriptionType }

export type projectFullResponse = { contractValues?: projectFullType | null, contractErrors?: string | null }

export async function readProjectCore(web3, contractAddress) {
    // Project-Data: Build and Call
    let contractMethods = ProjectModel.projectMethods(web3, contractAddress);
    var contractResults: projectFullResponse = await readContractData(web3, Smartraise, contractAddress, contractMethods)
    contractResults.contractValues.contractAddress = contractAddress;

    if (contractResults.contractValues) {

        // Load Phases 
        let contractPhaseMethods = PhasesModel.phaseMethods(web3, contractAddress);
        var phases = await PhasesModel.readPhaseData(web3, contractAddress, contractPhaseMethods, parseInt(contractResults.contractValues.countPhases))
        // TODO: Error handling
        contractResults.contractValues.phases = phases

        // Load Phases 
        // TODO: Error handling
        contractResults.contractValues.description = await DescriptionModel.readDescription(web3, contractAddress)


        console.log("readProjectCore" + JSON.stringify(contractResults))
    }



    return contractResults
}
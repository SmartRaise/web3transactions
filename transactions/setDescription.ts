import { TransactionManager } from "../transactionManager/TransactionManager.js";
import { DescriptionType } from './readDescription';


export async function setDescription(tM: TransactionManager, contractAddress, description: DescriptionType) {
    const contract = await tM.getSmartRaiseContract(contractAddress);
    const descriptionString = JSON.stringify(description)
    const transaction = contract.methods.setDescription(descriptionString);

    tM.processTransaction(transaction, "Set Description")
}
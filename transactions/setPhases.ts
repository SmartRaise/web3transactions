import Web3 = require("web3");
import { TransactionManager } from "../transactionManager/TransactionManager.js";

export type SetPhaseData =
    {
        label: string
        goal: number
        dueDate: Date
    }

export async function setPhases(tM: TransactionManager, contractAddress, setPhaseData: SetPhaseData[]) {
    
    // Map/transform to arrays, and correct formats which are expected by the transaction call

    let labelsArray = [];
    let goalsArray = [];
    let dueDateArray = [];

    setPhaseData.forEach(el => {
        labelsArray.push(Web3.utils.fromUtf8(el.label))
        goalsArray.push(Web3.utils.toWei(el.goal.toString(), "ether"))
        dueDateArray.push(el.dueDate.getTime() / 1000)
    });

    // TODO: HIGH Accumulated goal is wrong. 

    const contract = await tM.getSmartRaiseContract(contractAddress);

    const transaction = contract.methods.setPhases(labelsArray, goalsArray, dueDateArray)

    tM.processTransaction(transaction, "Set Phases")
}
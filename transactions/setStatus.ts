import { TransactionManager } from "../transactionManager/TransactionManager.js";

export async function setHold(tM: TransactionManager, contractAddress) {
    const contract = await tM.getSmartRaiseContract(contractAddress);
    const transaction = contract.methods.setHold();

    tM.processTransaction(transaction, "Set Status to Hold")
}

export async function setCollecting(tM: TransactionManager, contractAddress) {
    const contract = await tM.getSmartRaiseContract(contractAddress);
    const transaction = contract.methods.setCollecting();    
    
    tM.processTransaction(transaction, "Set Status to Collecting")
}